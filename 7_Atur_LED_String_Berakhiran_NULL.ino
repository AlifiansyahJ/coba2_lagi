/* Program mengatur LED dengan menggunakan string berakhiran NULL.
   Keterangan perintah :
   On untuk menghidupkan LED
   Off untuk mematikan LED
   Penulisan huruf besar ataupun kecil tidak masalah */
const int PIN_12 = 12;

void setup()
{
    Serial.begin(9600);
    pinMode(PIN_12, OUTPUT);
}

void loop()
{
    char perintah[20] = ""; // clear data
    char A[1];
    A[1] = 0; // Supaya setiap kali data-data array A dipindahkan ke array perintah, array perintah akan selalu diakhiri 0.
              // Tapi meskipun tidak mengakhiri dengan NULL, Arduino IDE secara otomatis akan menambahkannya.
              // Contoh : Saat pertama kali data-data array A dipindahkan ke array perintah, maka indeks 0 array perintah akan
              //          terisi dengan data dan indeks 1 akan terisi NULL. Proses selanjutnya, indeks 1 yang tadinya NULL,
              //          akan ditimpa oleh data lain dan indeks 2 akan NULL. Mengingat strcat adalah fungsi untuk menambahkan
              //          string sumber ke akhir string target dan menimpa NULL/0. Mungkin ini juga bener, soalnya ada varuabel pengganggu.
              // Hipotesis : Mungkin tujuan penambahan NULL di akhir array A supaya NULL tersebut ditimpa di proses selanjutnya.
              //             Sehingga yang ditimpa itu NULL nya bukan char nya. (Kemungkinan besar benar).
              // Hambatan : Tidak ada indikator di monitor yang membedakan mana NULL mana kosong.
              // Kenapa harus diakhiri NULL? Padahal kalau g pake juga kan tidak masalah
              // Kalau di C katanya buat bisa melakukan looping dengan simpel tanpa harus membandingkan.
              // Since 0 is considered boolean FALSE and all other values are considered TRUE. Contoh
              // char source[]="Test";  // Actually: T e s t \0 ('\0' is the NULL-character)
              // char dest[8];
              // int i=0;
              // char curr;
              // do {
              //    curr = source[i];
              //    dest[i] = curr;
              //    i++;
              //    } while(curr); // Perulangan akan dihentikan hanya jika curr = 0 (FALSE)
    while(Serial.available() != 0)
    {
        char kar = Serial.read();
        A[0] = kar;
        strcat(perintah, A);
        delay(20);
    }
       
    // Mengkapitalkan semua huruf kecil pada
    strupr(perintah);
    
    if(strcmp(perintah, "ON") == 0)
    {
        digitalWrite(PIN_12, HIGH);
        Serial.println("LED telah Dihidupkan");
        Serial.print("Indeks ke-0 : ");
        Serial.println(perintah[0]);
        Serial.print("Indeks ke-1 : ");
        Serial.println(perintah[1]);
        Serial.print("Indeks ke-2 : ");
        Serial.println(perintah[2]);
        Serial.print("Indeks ke-3 : ");
        Serial.println(perintah[3]);
    }
    else
    if(strcmp(perintah, "OFF") == 0)
    {
        digitalWrite(PIN_12, LOW);
        Serial.println("LED telah Dimatikan");
    }
}
