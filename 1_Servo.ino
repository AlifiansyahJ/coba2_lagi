#include <Servo.h>

const int PIN_SERVO = 11;

// Membuat objek
Servo motorServo;

void setup()
{
    // Menghubungkan motor servo ke pin servo
    motorServo.attach(PIN_SERVO);
}

void loop()
{
    motorServo.write(0);
    delay(2000);

    motorServo.write(90);
    delay(2000);

    motorServo.write(135);
    delay(2000);
}
